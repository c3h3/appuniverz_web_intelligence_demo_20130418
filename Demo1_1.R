library(png)
library(e1071)

data_pics <- c(paste("~/Pictures/b",1:5,".png",sep=""),paste("~/Pictures/d",1:5,".png",sep=""))


load("sample_data.RData")

par(mfrow = c(1, 3))

O_w = 1
X_w = 5
plotting_data = data.frame(data,Ws=c(rep(O_w,5),rep(X_w,5)))

svm_model = svm(z~.,data=data,kernel="linear",cost=10,class.weights=c(O=O_w,X=X_w))

# plot(data[,2:3], pch = 21, bg = c("red", "green3")[unclass(data$z)],cex = c(rep(O_w,5),rep(X_w,5)))


test_N = 100
test_s = (0:test_N)/test_N

test_x_M = max(data$x)+1
test_x_m = min(data$x)-1
test_x = test_x_m + (test_x_M - test_x_m)*test_s

test_y_M = max(data$y)+1
test_y_m = min(data$y)-1
test_y = test_y_m + (test_y_M - test_y_m)*test_s

test_data = expand.grid(test_x,test_y)
colnames(test_data)=c("x","y")

test_result = predict(svm_model,newdata=test_data, decision.values =TRUE)
test_result_decision_values = attributes(test_result)$decision.values
test_result_decision_values_M = matrix(test_result_decision_values,nrow=test_N+1)
test_result = predict(svm_model,newdata=test_data)

test_data_v = data.frame(test_data,z_v=test_result_decision_values)
colnames(test_data_v)[3]="z_v"

test_data = data.frame(test_data,z=test_result)

data_points = data[,-1]
data_points$z_v = as.numeric(data$z)*(-2)+3

total_data = rbind(data.frame(data_points,type="data"),data.frame(test_data_v,type="test"))

image(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1), col = topo.colors(50),sub="More Weights on Greens",xlab="x",ylab="y")
contour(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1),levels = seq(-1, 1, by = 1), add = TRUE, col = "peru",lwd=5)
points(data[,2:3], pch = 21, bg = c("red", "green3")[unclass(data$z)],cex = c(rep(O_w,5),rep(X_w,5)))

# points(data_points$x, data_points$y, pch=c(22,23,25)[unclass(data_points$z_v+2)], cex = c(rep(O_w,5),rep(X_w,5)))

# for (i in 1:NROW(plotting_data)){
#   picture_size <- plotting_data[i,]$Ws / 2
#   picture_size_switch <- picture_size / 2
#   qp = readPNG(source=data_pics[i])
#   plot_data = plotting_data[i,]
#   rasterImage(qp,
#               plot_data$x - picture_size_switch ,
#               plot_data$y - picture_size_switch ,
#               plot_data$x + picture_size_switch ,
#               plot_data$y + picture_size_switch ,
#               interpolate= FALSE)
# }


#########################################################################################

O_w = 1
X_w = 1
plotting_data = data.frame(data,Ws=c(rep(O_w,5),rep(X_w,5)))

svm_model = svm(z~.,data=data,kernel="linear",cost=10,class.weights=c(O=O_w,X=X_w))

# plot(data[,2:3], pch = 21, bg = c("red", "green3")[unclass(data$z)],cex = c(rep(O_w,5),rep(X_w,5)))


test_N = 100
test_s = (0:test_N)/test_N

test_x_M = max(data$x)+1
test_x_m = min(data$x)-1
test_x = test_x_m + (test_x_M - test_x_m)*test_s

test_y_M = max(data$y)+1
test_y_m = min(data$y)-1
test_y = test_y_m + (test_y_M - test_y_m)*test_s

test_data = expand.grid(test_x,test_y)
colnames(test_data)=c("x","y")

test_result = predict(svm_model,newdata=test_data, decision.values =TRUE)
test_result_decision_values = attributes(test_result)$decision.values
test_result_decision_values_M = matrix(test_result_decision_values,nrow=test_N+1)
test_result = predict(svm_model,newdata=test_data)

test_data_v = data.frame(test_data,z_v=test_result_decision_values)
colnames(test_data_v)[3]="z_v"

test_data = data.frame(test_data,z=test_result)

data_points = data[,-1]
data_points$z_v = as.numeric(data$z)*(-2)+3

total_data = rbind(data.frame(data_points,type="data"),data.frame(test_data_v,type="test"))

image(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1), col = topo.colors(50),sub="Equal Weighted",xlab="x",ylab="y")
contour(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1),levels = seq(-1, 1, by = 1), add = TRUE, col = "peru",lwd=5)
points(data[,2:3], pch = 21, bg = c("red", "green3")[unclass(data$z)],cex = c(rep(O_w,5),rep(X_w,5)))

# points(data_points$x, data_points$y, pch=c(22,23,25)[unclass(data_points$z_v+2)], cex = c(rep(O_w,5),rep(X_w,5)))

# for (i in 1:NROW(plotting_data)){
#   picture_size <- plotting_data[i,]$Ws / 2
#   picture_size_switch <- picture_size / 2
#   qp = readPNG(source=data_pics[i])
#   plot_data = plotting_data[i,]
#   rasterImage(qp,
#               plot_data$x - picture_size_switch ,
#               plot_data$y - picture_size_switch ,
#               plot_data$x + picture_size_switch ,
#               plot_data$y + picture_size_switch ,
#               interpolate= FALSE)
# }


#########################################################################################


O_w = 5
X_w = 1
plotting_data = data.frame(data,Ws=c(rep(O_w,5),rep(X_w,5)))
svm_model = svm(z~.,data=data,kernel="linear",cost=10,class.weights=c(O=O_w,X=X_w))

test_N = 100
test_s = (0:test_N)/test_N

test_x_M = max(data$x)+1
test_x_m = min(data$x)-1
test_x = test_x_m + (test_x_M - test_x_m)*test_s

test_y_M = max(data$y)+1
test_y_m = min(data$y)-1
test_y = test_y_m + (test_y_M - test_y_m)*test_s

test_data = expand.grid(test_x,test_y)
colnames(test_data)=c("x","y")

test_result = predict(svm_model,newdata=test_data, decision.values =TRUE)
test_result_decision_values = attributes(test_result)$decision.values
test_result_decision_values_M = matrix(test_result_decision_values,nrow=test_N+1)
test_result = predict(svm_model,newdata=test_data)

test_data_v = data.frame(test_data,z_v=test_result_decision_values)
colnames(test_data_v)[3]="z_v"

test_data = data.frame(test_data,z=test_result)

data_points = data[,-1]
data_points$z_v = as.numeric(data$z)*(-2)+3

total_data = rbind(data.frame(data_points,type="data"),data.frame(test_data_v,type="test"))

image(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1), col = topo.colors(50),sub="More Weights on Reds",xlab="x",ylab="y")
contour(test_x, test_y, matrix(test_data_v$z_v,nrow=test_N+1),levels = seq(-1, 1, by = 1), add = TRUE, col = "peru",lwd=5)
points(data[,2:3], pch = 21, bg = c("red", "green3")[unclass(data$z)],cex = plotting_data$Ws)



# for (i in 1:NROW(plotting_data)){
#   picture_size <- plotting_data[i,]$Ws / 2
#   picture_size_switch <- picture_size / 2
#   qp = readPNG(source=data_pics[i])
#   plot_data = plotting_data[i,]
#   rasterImage(qp,
#               plot_data$x - picture_size_switch ,
#               plot_data$y - picture_size_switch ,
#               plot_data$x + picture_size_switch ,
#               plot_data$y + picture_size_switch ,
#               interpolate= FALSE)
# }




