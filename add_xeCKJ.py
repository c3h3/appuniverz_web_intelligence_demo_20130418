# -*- coding: utf-8 -*-

import sys

filename = sys.argv[1]

##filename = "test3.tex"

file_obj = open(filename,"r")

file_obj_reads = file_obj.read()

print type(file_obj_reads) 
print len(file_obj_reads)

results = """\usepackage{Sweave}\n\usepackage{fontspec}\n\usepackage{xeCJK}\n\setCJKmainfont{WenQuanYi Micro Hei}\n""".join(file_obj_reads.split("\usepackage{Sweave}"))

file_obj.close()

file_obj = open(filename,"w")
file_obj.write(results)
file_obj.close()

##file_obj_reads.split(unicode("\usepackage{Sweave}"))


